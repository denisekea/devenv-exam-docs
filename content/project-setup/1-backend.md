---
title: 'Backend'
metaTitle: 'Backend set-up'
metaDescription: 'This is the meta description for this page'
---

The backend runs on Node.js and Express. Check if you have Node.js installed by running `node -v`.

### Navigate to backend

```
cd project/api
```

### Install node modules

```
npm i
```

### Run backend locally

```
node app.js
```

This will run the Express app on <http://localhost:8080>

To see live changes during development you can also run it with nodemon.
Install nodemon globally with `npm i -g nodemon` and run the project with:

```
nodemon app.js
```
