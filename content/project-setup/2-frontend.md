---
title: 'Frontend'
metaTitle: 'Frontend set-up'
metaDescription: 'This is the meta description for this page'
---

The frontend is built with React.

### Navigate to frontend

```
cd project/todo_app
```

### Install node modules

```
npm i
```

### Run frontend locally

```
npm start
```

This will open the project on <http://localhost:3000>

### Run build for production

```
npm run build
```

### Run ESLint

```
npm run lint
```

### Run tests with Jest

```
npm run test
```
