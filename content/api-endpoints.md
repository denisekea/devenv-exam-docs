---
title: 'API endpoints'
metaTitle: 'API endpoints'
metaDescription: 'List of all API end points'
---

Here you can find all end points we use in our app.

## Authentication

| Endpoint            | Method | Description      | Request body                                   |
| ------------------- | ------ | ---------------- | ---------------------------------------------- |
| /session/getcurrent | GET    | get current user |                                                |
| /auth/login         | POST   | login user       | { email, password }                            |
| /auth/signup        | POST   | add new user     | { email, password, confirmPassword, fullName } |
| /auth/:userid       | DELETE | delete user      |                                                |

## Lists

| Endpoint               | Method | Description                    | Request body               |
| ---------------------- | ------ | ------------------------------ | -------------------------- |
| /lists/                | GET    | all lists                      |                            |
| /lists/add             | POST   | add new list                   | { listName: string }       |
| /lists/:listid         | POST   | add new item to list           | { newItemName: string }    |
| /lists/:listid/:itemid | PUT    | change item's completed status | { itemCompleted: boolean } |
| /lists/:listid/:itemid | PATCH  | delete item                    |                            |
| /lists/:listid         | DELETE | delete list                    |                            |
