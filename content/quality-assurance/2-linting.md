---
title: 'Linting'
metaTitle: 'Linting'
metaDescription: 'This is the meta description for this page'
---

## ESLint


Install ESLint locally or globally by running command: 

in the workspace folder for a local install 
```
npm install eslint 
```
For a global install
```
npm -g install eslint
```
## Prettier extension ?
