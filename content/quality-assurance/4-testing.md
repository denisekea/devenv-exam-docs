---
title: 'Testing'
metaTitle: 'Testing'
metaDescription: 'This is the meta description for this page'
---

As the project was created with create-react-app, jest is already included with useful defaults. 

## What should be tested

- Check if components render correctly. It is important to catch any JSX syntax errors and investigate if the component renders what it is supposed - to.
- Form validation - test should investigate errors handling.
- Test states of the components, check if conditions work and render appropriate content correctly. 


## Filename Conventions


Create a test file in the same directory as the file/component you want to test. 
All test files should be named with **.test.js** suffix

Example: If you want to test **example.js** file - create a test file **example.test.js** in the same directory as **example.js** file. 

To run test use command line interface - jest will launch in watch mode.  
```
npm run test
```
The watcher includes an interactive command-line interface with the ability to run all tests, or focus on a search pattern. It is designed this way so that you can keep it open and enjoy fast re-runs. You can learn the commands from the “Watch Usage” note that the watcher prints after every run.


## Testing Recipes

To create tests files follow Testing Recipes from official React documentation: 
https://reactjs.org/docs/testing-recipes.html
