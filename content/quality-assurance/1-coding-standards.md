---
title: 'Coding standards'
metaTitle: 'Coding standards'
metaDescription: 'This is the meta description for this page'
---

## General rules
- DRY principle (Don’t Repeat Yourself): try to avoid duplicate code, try to make functions and components reusable
- Single responsibility principle: each component should only have one responsibility


## React specific rules
- Use functional React components with React Hooks, no class components with
lifecycle methods anymore.
- Create a separate folder for each route in the application
- The order of imports:
1. React import
2. Library imports
3. Absolute imports from the project
4. Relative imports
5. Import 
```
./<some file>.<some extension>
```
- Always destructure props before use in components
- Avoid inline styling, use the Styled Components library.2​ 1 Styled Components let’s
you write css as a React component. Naming Conventions
- PascalCase for components and component file names, e.g. N​ avBar, NavBar.jsx
- camelCase for functions, props (except if props contains a component) and other
JS logic, e.g. ​handleFormSubmit Coding style
- Follow the Airbnb Style Guide​: https://github.com/airbnb/javascript/tree/master/react
